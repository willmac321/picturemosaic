#!/usr/bin/python3
import numpy as np
import sys
from operator import itemgetter
import RankColors
from PIL import Image, ImageDraw, ImageFont

fileToTile = '0.jpg'
resize = 2
tileS = (50,50)
def main():
    tiles = RankColors.rankedImages('Images\\', tileS, '.jpg', fileToTile)

    # for item in tiles.listOfImages:
    #     print(item.name, " : ", item.sumOfRgb, " : ", item.rgb )
    x, y = Image.open('Images\\'+fileToTile).size
    x = x * resize
    y = y * resize
    area = x * y
    print(x, y, area)
    mosaic = RankColors.tiledImage('Images\\'+fileToTile, area, tileS, (x, y))
    mosaic = minDistanceTiles(mosaic, tiles)
    i = 0

    imgOut = Image.new('RGB', (x, y), 'white')

    for item in mosaic.listOfImages:
        imgIn = Image.open("Images/" + item.name, 'r').resize(tileS)
        imgOut.paste(imgIn, (item.x, item.y))
    imgOut.save('out.png')

        # print(item.name, " : mosaic", item.rgb , " :",tiles.listOfImages[i].name," : ", tiles.listOfImages[i].rgb, " : dist", distance(item.rgb, tiles.listOfImages[i].rgb))
        # i += 1

    # for item in mosaic.listOfImages:
    #     print(item.name, " : mosaic", item.rgb , " :",tiles.listOfImages[0].name," : ", tiles.listOfImages[0].rgb, " : dist", distance(item.rgb, tiles.listOfImages[0].rgb))

#destination is tile to replace
#source is tile to do replacing
def minDistanceTiles(listDest, listSource):
    for i in range(len(listDest.listOfImages)):
        dist = 0
        oldMinCount = float('inf')
        newDist = float('inf')
        destRGB = listDest.listOfImages[i].rgb
        t = 0
        for j in range(len(listSource.listOfImages)):
            dist = distance(destRGB, listSource.listOfImages[j].rgb)

            #if distance is at a minimum check if count is less than previous minimum

            if dist < newDist and listSource.listOfImages[j].useCount < oldMinCount:
                    t = j
                    newDist = dist
                    oldMinCount = listSource.listOfImages[j].useCount


        listDest.listOfImages[i].name = listSource.listOfImages[t].name
        listSource.listOfImages[t].useCount += 1
    # b = listSource.listOfImages
    # sorted(b, key = lambda x : x.name)
    # for l in b:
    #     print(l.name, " " ,l.useCount)
                #print(oldMinCount)
    return listDest
#destination is tile to replace
#source is tile to do replacing
def distance(rgbDest, rgbSource):
    return (rgbDest[0] - rgbSource[0]) ** 2 + (rgbDest[1] - rgbSource[1]) ** 2 + (rgbDest[2] - rgbSource[2]) ** 2

if __name__ == "__main__":
    main()

            # print(arr[x][y][0])
            # print(arr[x][y][1])
            # print(arr[x][y][2])
            # print(arr[x][y][3])
#
#
# if __name__ == "__main__":
#     print("please input an image file path as first arg, 'path.png'")
#     print("also optionally supply the x and y dimensions to resize image to")
#     print("['image.png'] [Scale\{decimal\}] [rotate=True] [minAscii] [maxAscii] [fontScaling] [addSpaceAsWhite]\n")
#     # execute only if run as a script
#     if(len(sys.argv[1:]) > 0 and len(sys.argv[1:]) < 3):
#         main(sys.argv[1], float(sys.argv[2]))
#     elif(len(sys.argv[1:]) > 0 and len(sys.argv[1:]) < 4):
#         main(sys.argv[1], float(sys.argv[2]), sys.argv[3])
#     elif(len(sys.argv[1:]) > 0 and len(sys.argv[1:]) < 8):
#         main(sys.argv[1], float(sys.argv[2]), sys.argv[3], int(sys.argv[4]), int(sys.argv[5]), int(sys.argv[6]), sys.argv[7])
