
import os, os.path
from PIL import Image
from math import isclose

#base image class
class rgbImage:

    def __init__(self, image, size, name):
        self.useCount = 0
        self.rgb = (0,0,0)
        self.s = size
        self.sumOfRgb = 0
        self.name = name
        #start x and y pixel if not 0 (top left corner)
        self.x = 0
        self.y = 0
        self.img = image.resize(self.s)
        self.findAvgRGB()

    def findAvgRGB(self):
        #https://stackoverflow.com/questions/12703871/benchmarking-functions-for-computing-average-rgb-value-of-images
        img = self.img
        np = img.size[0] * img.size[1]
        col = img.getcolors(np)
          # calculate (sum(ci*ri)/np, sum(ci*gi)/np, sum(ci*bi)/np)
          # the zip gives us [(c1*r1, c2*r2, ..), (c1*g1, c1*g2,...)...]
        sumRGB = [(x[0]*x[1][0], x[0]*x[1][1], x[0]*x[1][2]) for x in col]
        avg = tuple([sum(x)/np for x in zip(*sumRGB)])
        img.close()
        self.rgb = avg
        self.sumOfRgb = avg[0] + avg[1] + avg[2]
        return avg

    def useImage(self):
        self.useCount += 1
        return self.img

    def setX(self, x):
        self.x = x
    def setY(self, y):
        self.y = y
#
#1. resize image(s) to supplied size
#2. create list to store average RGB value of image(s)
#3. order list
#getter to retrieve list that has object containing ref to image, rgb average, and int for use count

#creates list of rgbImage that is tiles of mosaic
class rankedImages:
    def __init__(self, directoryPath, size, fileExtension, skipFile):
        self.dP = directoryPath;
        self.listOfImages = list()
        self.fE = fileExtension
        self.size = size
        self.skipFile = skipFile

        self.createImageList()
    #get directory contianing images

    def createImageList(self):
        # tst = rgbImage(dP + '\\113.png', (100,100))
        # print(tst.findAvgRGB())

        i = 0
        for name in os.listdir(self.dP):
            if name.endswith(self.fE) and not name.__contains__(self.skipFile):
                img = Image.open(self.dP + name).convert("RGB")
                img.thumbnail(self.size)
                img.save(self.dP + name)
                item = rgbImage(Image.open(self.dP + name), self.size, name)
                # print(str(i),": ",dP + name, ": ",item.rgb)
                self.listOfImages.append(item)
                i += 1

        self.listOfImages.sort(key = lambda x : x.sumOfRgb)

#creates list of rgbImage that is tiles of main larger image
class tiledImage:
    def __init__(self, fileName, tileArea, tileSize, imageResize):
        self.fileName = fileName
        self.tileA = tileArea
        self.tileS = tileSize
        self.listOfImages = list()
        self.imageResize = imageResize
        self.splitUpImage()
        #"""take the most current image then split into number of tiles supplied and create ranked RGB list with it"""
    def splitUpImage(self):
        listTiles = list()
        img = Image.open(self.fileName).convert("RGB")
        img = img.resize(self.imageResize)
        iX, iY = img.size
        print(img.size)
        area = iX * iY
        count = area / self.tileA

        if not isclose(count, round(count)):
            #resize image here to accomodate sizing issues
            print(int(round(iX / (self.tileA / 100))) , int(round(iY / (self.tileA / 100))))

        for y in range(0, iY, self.tileS[1]):
            for x in range(0, iX, self.tileS[0]):
                cr = img.crop((x, y, x + self.tileS[0], y + self.tileS[1]))
                item = rgbImage(cr, cr.size, str(x) + "," + str(y))
                item.setX(x)
                item.setY(y)
                self.listOfImages.append(item)
