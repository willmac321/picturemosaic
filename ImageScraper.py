import shutil
import requests
import urllib.request
import time
from PIL import Image
import glob, os
#https://kaijento.github.io/2017/05/06/web-scraping-nasa-image-of-the-day/

file_path = 'Images\\'

api    = 'https://www.nasa.gov/api/1/'
public = 'https://www.nasa.gov/sites/default/files'


with requests.session() as s:
    s.headers['user-agent'] = 'Mozilla/5.0'
    i=0
    # first 2 pages
    for page in range(30):
        r = s.get(api + 'query/ubernodes.json',
                params={'page': page, 'unType[]': 'image'})
        for ubernode in r.json()['ubernodes']:
            nid = ubernode['nid']
            r = s.get(api + 'record/node/{}.json'.format(nid))
            for image in r.json()['images']:
                uri = image['uri'].replace('public:/', public, 1)
                with urllib.request.urlopen(uri) as response:
                    fp = file_path+'\\' + str(i) + '.jpg'
                    with open(fp, 'wb') as tmp_file:
                        try:
                            shutil.copyfileobj(response, tmp_file)
                            tmp_file.close()
                        except:
                            tmp_file.close()
                            os.remove(tmp_file)
                        i += 1


                        time.sleep(.1)
                if not (i == 1):
                    try:
                        img =  Image.open(fp).convert('RGB')
                        img.thumbnail((150, 150))
                        img.save(fp)
                        img.close()
                    except:
                        print("Issue with " + str(fp))
        print(page)





                # code = urllib.request.urlopen(uri)
                # meBytes = code.read()
                # meString = meBytes.decode("utf8")
                # code.close()
                # print(meString)

#
# i = 0
# for i in range(0,1):

#
# print(i)
